package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type task struct { // defimos como sera la estructura de los elementos con nombre tarea
	Id      int    `json:Id`
	Name    string `json:Name`
	Content string `json:Content`
}

type allTasks []task

var tasks = allTasks{
	{
		Id:      1,
		Name:    "task one",
		Content: "some Content",
	},
}

func getTasks(w http.ResponseWriter, r *http.Request) { // listar todas las tareas
	j, err := json.Marshal(tasks)
	if err != nil {
		fmt.Printf("Error: %s", err.Error())
	} else {
		fmt.Println(string(j))
		w.Header().Set("Content-type", "application/json")
		json.NewEncoder(w).Encode(tasks)
	}
}

func createTask(w http.ResponseWriter, r *http.Request) { // crear nueva tarea
	var newTask task
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "inserte una tarea valida")
	}
	fmt.Println("into")
	//fmt.Printf("%v", reqBody)
	json.Unmarshal(reqBody, &newTask)                  //asignamos la informacion
	newTask.Id = len(tasks) + 1                        // generando un nuevo id // con la longitud del array y sumando mas 1
	tasks = append(tasks, newTask)                     //guardamos la nueva tarea en el array
	w.Header().Set("content-type", "application/json") // cabecera respuesta http que le damos al cliente
	w.WriteHeader(http.StatusCreated)                  // codigo de estado  que se creo correctamente
	json.NewEncoder(w).Encode(newTask)                 // responde lo que guardamos
}

//Eliminar tarea
func deleteTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)                     // recuperando todas las variables que lleguen por url
	taskID, err := strconv.Atoi(vars["id"]) // obtengo especificamente la variable id
	if err != nil {
		fmt.Fprintf(w, "id invalido")
	}

	for i, task := range tasks {
		if (task.Id) == taskID { // comparamos el id que nos llega por url y buscamos en el array
			tasks = append(tasks[:i], tasks[i+1:]...)                            //[a,b,c] = [,b,c] = [b,c] // eliminamos el id encontrado
			fmt.Fprintf(w, "La tarea con id %v fue eliminado con exito", taskID) // respondemos que el id fue eliminado
		}
	}
}

//Actualziar tarea
func updateTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)                     // recuperando todas las variables que lleguen por url
	taskID, err := strconv.Atoi(vars["id"]) // obtengo especificamente la variable id
	var updateTask task

	if err != nil {
		fmt.Fprintf(w, "id invalido")
	}

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "por ingrese datos correctos")
	}

	json.Unmarshal(reqBody, &updateTask)

	for i, task := range tasks {
		if (task.Id) == taskID { // comparamos el id que nos llega por url y buscamos en el array
			updateTask.Id = taskID
			tasks[i] = updateTask
			fmt.Fprintf(w, "La tarea con id %v fue actualizado con exito", taskID) // respondemos que el id fue eliminado
		}
	}
}

//Recuperar tarea especifica
func getTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)                     // recuperando todas las variables que lleguen por url
	taskID, err := strconv.Atoi(vars["id"]) // obtengo especificamente la variable id
	if err != nil {
		fmt.Fprintf(w, "id invalido")
	}

	for _, task := range tasks {
		if (task.Id) == taskID { // comparamos el id que nos llega por url y buscamos en el array
			w.Header().Set("Content-type", "application/json")
			json.NewEncoder(w).Encode(task)
		}
	}
}

func indexRouter(w http.ResponseWriter, r *http.Request) {
	// w -> es la respuesta que data nuestro servicio REST
	// r -> es la peticion y lo que envia el usuario-cliente
	fmt.Fprint(w, "welcome my Rest Api - Tecnologias Emergentes - hola mundo")
}

func main() {
	//fmt.Println("hello word")
	//
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", indexRouter)
	router.HandleFunc("/all-tasks", getTasks).Methods("GET")
	router.HandleFunc("/create-tasks", createTask).Methods("POST")
	router.HandleFunc("/remove-tasks/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/task/{id}", getTask).Methods("GET")
	router.HandleFunc("/update-task/{id}", updateTask).Methods("PUT")
	server := http.ListenAndServe(":3300", router) //estoy creand un servidor que escuche en el puerto 3000
	log.Fatal(server)                              // vere logs del servidor
}
