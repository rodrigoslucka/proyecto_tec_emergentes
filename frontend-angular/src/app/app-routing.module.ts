import { NgModule } from '@angular/core';
import { ChildrenOutletContexts, RouterModule, Routes } from '@angular/router';
import { SkeletonComponent } from '@layout/skeleton/skeleton.component';
import { LoginComponent } from '@modules/auth/login/login.component';
import { SkeletonDashboardComponent } from '@modules/dashboard/skeleton-dashboard/skeleton-dashboard.component';

const routes: Routes = [
  {
    path:'',//ruta directa  -> / -> raiz
    component:SkeletonComponent,
    children:[
      {
        path:'', // pasa a la primera ruta que este en las rutas hijas
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/home/home.module').then((m)=>m.HomeModule)
      },
      {
        path:'tabs', 
        loadChildren:()=>
          import('@modules/tabs/tabs.module').then((m)=>m.TabsModule)
      },         
    ]
  },
  {
    path:'auth',
    component:LoginComponent,
    children:[
      {
        path:'',
        loadChildren:()=>//importar primero el modulo para poder llamarlo
          import('@modules/auth/auth.module').then((m)=>m.AuthModule)
      }      
    ]    
  },
  {
    path:'dashboard',
    component:SkeletonDashboardComponent,
    children:[
      {
        path:'home',
        loadChildren:()=>
          import('@modules/dashboard/dashboard.module').then((m)=>m.DashboardModule)
      },                
    ]
  }
];
//{useHash:true} -> quita el # de la url
@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
