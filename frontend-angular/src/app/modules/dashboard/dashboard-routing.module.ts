import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { OrdersComponent } from './orders/orders.component';
import { ViewCategoryComponent } from './view-category/view-category.component';


const routes: Routes = [
    {
        path:'',// principal
        component:HomeDashboardComponent
    },
    {
      path:'Create',// principal
      component:CreateComponent
   },
    {
      path:'Edit',// principal
      component:EditComponent
    },
    {
      path:'Orders',// principal
      component:OrdersComponent
  },
  {
    path:'View',// principal
    component:ViewCategoryComponent
}
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
