import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar-dashboard',
  templateUrl: './navbar-dashboard.component.html',
  styleUrls: ['./navbar-dashboard.component.css']
})
export class NavbarDashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public openSiderbar(){
    var elementSidebar = document.getElementById("sidebar")
    var elementButton = document.getElementById("buttonsidebar")
    elementSidebar.classList.add('active')
    elementButton.style.visibility = "hidden"
  } 
  public closeSiderbar(){
    var elementSidebar = document.getElementById("sidebar")
    var elementButton = document.getElementById("closesidebar")
    elementSidebar.classList.remove('active')
    elementButton.style.visibility = "visible"    
  } 
}
