import { NgModule } from '@angular/core';
import { HomeDashboardComponent } from './home-dashboard/home-dashboard.component';
import { NavbarDashboardComponent } from './navbar-dashboard/navbar-dashboard.component';
import { SkeletonDashboardComponent } from './skeleton-dashboard/skeleton-dashboard.component';
import { SharedModule } from '@shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { OrdersComponent } from './orders/orders.component';
import { ViewCategoryComponent } from './view-category/view-category.component';



@NgModule({
  declarations: [
    HomeDashboardComponent,
    NavbarDashboardComponent,    
    SkeletonDashboardComponent,
    EditComponent,
    CreateComponent,
    OrdersComponent,
    ViewCategoryComponent
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ]
})
export class DashboardModule { }
