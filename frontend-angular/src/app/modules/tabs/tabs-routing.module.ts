import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormComponent } from './form/form.component';
import { GalleryComponent } from './gallery/gallery.component';
import { TabsModule } from './tabs.module';



const routes: Routes = [
    {
        path:'gallery',
        component:GalleryComponent
    },
    {
        path:'form',
        component:FormComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsRoutingModule { }
