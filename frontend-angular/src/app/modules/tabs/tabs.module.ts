import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryComponent } from './gallery/gallery.component';
import { SharedModule } from '@shared/shared.module';
import { FormComponent } from './form/form.component';
import { TabsRoutingModule} from './tabs-routing.module'


@NgModule({
  declarations: [
    GalleryComponent,
    FormComponent
  ],
  imports: [
    SharedModule,
    TabsRoutingModule
  ]
})
export class TabsModule { }
