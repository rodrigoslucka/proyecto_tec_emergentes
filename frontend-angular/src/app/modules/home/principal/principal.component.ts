import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css'],
  encapsulation:ViewEncapsulation.None  
})
export class PrincipalComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
